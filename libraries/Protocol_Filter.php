<?php

/**
 * Protocol filter app powered by Netify.
 *
 * @category   apps
 * @package    protocol-filter
 * @subpackage libraries
 * @author     eGloo <team@egloo.ca>
 * @copyright  2016 eGloo
 * @license    http://www.gnu.org/copyleft/lgpl.html GNU Lesser General Public License version 3 or later
 * @link       https://www.netify.ai/resources/platforms/clearos
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// N A M E S P A C E
///////////////////////////////////////////////////////////////////////////////

namespace clearos\apps\protocol_filter;

///////////////////////////////////////////////////////////////////////////////
// B O O T S T R A P
///////////////////////////////////////////////////////////////////////////////

$bootstrap = getenv('CLEAROS_BOOTSTRAP') ? getenv('CLEAROS_BOOTSTRAP') : '/usr/clearos/framework/shared';
require_once $bootstrap . '/bootstrap.php';

///////////////////////////////////////////////////////////////////////////////
// T R A N S L A T I O N S
///////////////////////////////////////////////////////////////////////////////

clearos_load_language('base');
clearos_load_language('samba');
clearos_load_language('samba_common');

///////////////////////////////////////////////////////////////////////////////
// D E P E N D E N C I E S
///////////////////////////////////////////////////////////////////////////////

// Classes
//--------

use \clearos\apps\netify_fwa\Netify_FWA as Netify_FWA;

clearos_load_library('netify_fwa/Netify_FWA');

///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * Protocol filter app powered by Netify.
 *
 * @category   apps
 * @package    protocol-filter
 * @subpackage libraries
 * @author     eGloo <team@egloo.ca>
 * @copyright  2016 eGloo
 * @license    http://www.gnu.org/copyleft/lgpl.html GNU Lesser General Public License version 3 or later
 * @link       https://www.netify.ai/resources/platforms/clearos
 */

class Protocol_Filter extends Netify_FWA
{
    ///////////////////////////////////////////////////////////////////////////////
    // M E T H O D S
    ///////////////////////////////////////////////////////////////////////////////

    /**
     * Protocol filter constructor.
     *
     * @return void
     */

    public function __construct()
    {
        clearos_profile(__METHOD__, __LINE__);

        parent::__construct('netify-fwa');
    }

    /**
     * Adds white list IP.
     *
     * @param string $ip IP address
     */

    public function add_whitelist_ip($ip)
    {
        clearos_profile(__METHOD__, __LINE__);

        return $this->_add_whitelist_ip(self::TYPE_PROTOCOL, $ip);
    }

    /**
     * Deletes white list IP.
     *
     * @param string $ip IP address
     */

    public function delete_whitelist_ip($ip)
    {
        clearos_profile(__METHOD__, __LINE__);

        return $this->_delete_whitelist_ip(self::TYPE_PROTOCOL, $ip);
    }

    /**
     * Returns IP whitelist.
     */

    public function get_ip_whitelist()
    {
        clearos_profile(__METHOD__, __LINE__);

        return $this->_get_whitelist(self::TYPE_PROTOCOL, self::NETWORK_LOCAL_IP);
    }

    /**
     * Returns MAC whitelist.
     */

    public function get_mac_whitelist()
    {
        clearos_profile(__METHOD__, __LINE__);

        return $this->_get_whitelist(self::TYPE_PROTOCOL, self::NETWORK_LOCAL_MAC);
    }

    /**
     * Returns rules.
     */

    public function get_rules()
    {
        clearos_profile(__METHOD__, __LINE__);

        $rules = $this->_get_rules(self::TYPE_PROTOCOL);

        // Skip "Unknown" protocol in this app, not really applicable
        unset($rules[0]);

        return $rules;
    }

    /**
     * Returns state of filter.
     */

    public function get_state()
    {
        clearos_profile(__METHOD__, __LINE__);

        return $this->_get_state(self::TYPE_PROTOCOL);
    }

    /**
     * Sets rules.
     */

    public function set_rules($rules)
    {
        clearos_profile(__METHOD__, __LINE__);

        return $this->_set_rules(self::TYPE_PROTOCOL, $rules);
    }

    /**
     * Sets state of filter.
     *
     * @param boolean $state state of filter
     */

    public function set_state($state)
    {
        clearos_profile(__METHOD__, __LINE__);

        return $this->_set_state(self::TYPE_PROTOCOL, $state);
    }

    /**
     * Updates state of filter after configuration change.
     */

    public function update_state()
    {
        clearos_profile(__METHOD__, __LINE__);

        $this->_update_state();
    }
}
