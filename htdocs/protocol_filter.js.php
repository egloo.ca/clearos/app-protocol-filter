<?php

/**
 * Protocol filter javascript helper.
 *
 * @category   apps
 * @package    protocol-filter
 * @subpackage javascript
 * @author     eGloo <team@egloo.ca>
 * @copyright  2016 eGloo
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       https://www.netify.ai/resources/platforms/clearos
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.  
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// B O O T S T R A P
///////////////////////////////////////////////////////////////////////////////

$bootstrap = getenv('CLEAROS_BOOTSTRAP') ? getenv('CLEAROS_BOOTSTRAP') : '/usr/clearos/framework/shared';
require_once $bootstrap . '/bootstrap.php';

///////////////////////////////////////////////////////////////////////////////
// J A V A S C R I P T
///////////////////////////////////////////////////////////////////////////////

// The following changes should be pushed to the theme
//
// TODO #1: the theme does not handle table groupings on summary tables
// properly.  To see this in action, disable the block of code below
// and compare to the list table in the Intrusion Detection app.
// Sorting on a column behaves differentl.
// 
// TODO #2: hide the table (via protocol_summary_wrapper) until it is
// drawn, othwerise we get a weird animation before datatables has grouped
// the data (Firefox only?).

header('Content-Type:application/x-javascript');
?>

$(document).ready(function() {
    $('#protocol_summary').on('draw.dt', function () {
        $("th").removeClass("sorting")
        $("th").unbind('click');

        $('#protocol_summary_wrapper').removeClass("hide");
        $('#protocol_white_list_wrapper').removeClass("hide");
    } );

    $('#protocol_edit').on('draw.dt', function () {
        $('#protocol_edit_wrapper').removeClass("hide");
    } );
});

// vim: ts=4 syntax=javascript
