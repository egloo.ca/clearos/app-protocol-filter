
Name: app-protocol-filter
Epoch: 1
Version: 2.3.23
Release: 1%{dist}
Summary: Protocol Filter
License: GPLv3
Group: Applications/Apps
Packager: eGloo
Vendor: eGloo
Source: %{name}-%{version}.tar.gz
Buildarch: noarch
Requires: %{name}-core = 1:%{version}-%{release}
Requires: app-base
Requires: app-network
Requires: theme-clearos-admin >= 7.1.18

%description
The Protocol Filter feature is used to block unwanted traffic from your network. The feature is commonly used to make sure employees, students or end users are using their Internet access for its intended use.

%package core
Summary: Protocol Filter - API
License: LGPLv3
Group: Applications/API
Requires: app-base-core
Requires: app-firewall-core
Requires: app-network-core
Requires: app-netify-core >= 1:2.3.0
Requires: app-netify-fwa-core >= 1:2.3.0

%description core
The Protocol Filter feature is used to block unwanted traffic from your network. The feature is commonly used to make sure employees, students or end users are using their Internet access for its intended use.

This package provides the core API and libraries.

%prep
%setup -q
%build

%install
mkdir -p -m 755 %{buildroot}/usr/clearos/apps/protocol_filter
cp -r * %{buildroot}/usr/clearos/apps/protocol_filter/


%post
logger -p local6.notice -t installer 'app-protocol-filter - installing'

%post core
logger -p local6.notice -t installer 'app-protocol-filter-api - installing'

if [ $1 -eq 1 ]; then
    [ -x /usr/clearos/apps/protocol_filter/deploy/install ] && /usr/clearos/apps/protocol_filter/deploy/install
fi

[ -x /usr/clearos/apps/protocol_filter/deploy/upgrade ] && /usr/clearos/apps/protocol_filter/deploy/upgrade

exit 0

%preun
if [ $1 -eq 0 ]; then
    logger -p local6.notice -t installer 'app-protocol-filter - uninstalling'
fi

%preun core
if [ $1 -eq 0 ]; then
    logger -p local6.notice -t installer 'app-protocol-filter-api - uninstalling'
    [ -x /usr/clearos/apps/protocol_filter/deploy/uninstall ] && /usr/clearos/apps/protocol_filter/deploy/uninstall
fi

exit 0

%files
%defattr(-,root,root)
/usr/clearos/apps/protocol_filter/controllers
/usr/clearos/apps/protocol_filter/htdocs
/usr/clearos/apps/protocol_filter/views

%files core
%defattr(-,root,root)
%exclude /usr/clearos/apps/protocol_filter/packaging
%exclude /usr/clearos/apps/protocol_filter/unify.json
%dir /usr/clearos/apps/protocol_filter
/usr/clearos/apps/protocol_filter/deploy
/usr/clearos/apps/protocol_filter/language
/usr/clearos/apps/protocol_filter/libraries
