<?php

$lang['protocol_filter_app_name'] = 'Protocol Filter';
$lang['protocol_filter_app_description'] = 'The Protocol Filter feature is used to block unwanted traffic from your network. The feature is commonly used to make sure employees, students or end users are using their Internet access for its intended use.';
