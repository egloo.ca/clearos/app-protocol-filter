<?php

/**
 * Netify Protocol Filter controller.
 *
 * @category   apps
 * @package    protocol-filter
 * @subpackage controllers
 * @author     eGloo <team@egloo.ca>
 * @copyright  2016 eGloo
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       https://www.netify.ai/resources/platforms/clearos
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// D E P E N D E N C I E S
///////////////////////////////////////////////////////////////////////////////

use \clearos\apps\base\Daemon as Daemon_Class;

require clearos_app_base('base') . '/controllers/daemon.php';

///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * Netify Protocol Filter controller.
 *
 * @category   apps
 * @package    protocol-filter
 * @subpackage controllers
 * @author     eGloo <team@egloo.ca>
 * @copyright  2016 eGloo
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       https://www.netify.ai/resources/platforms/clearos
 */

class Server extends Daemon
{
    /**
     * Netify daemon constructor.
     *
     * @return view
     */

    function __construct()
    {
        parent::__construct('netify-fwa', 'netify-fwa');
    }

    /**
     * Default controller.
     *
     * @return view
     */

    function index()
    {
        // Load dependencies
        //------------------

        $this->lang->load('base');

        $data['daemon_name'] = 'netify-fwa';
        $data['app_name'] = 'protocol_filter';

        // Load views
        //-----------

        $options['javascript'] = array(clearos_app_htdocs('base') . '/daemon.js.php');

        $this->page->view_form('base/daemon', $data, lang('base_server_status'), $options);
    }

    /**
     * Status.
     *
     * @return view
     */

    function status()
    {
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');

        $this->load->library('netify_fwa/Netify_FWA');
        $this->load->library('protocol_filter/Protocol_Filter');

        $filter_enabled = $this->protocol_filter->get_state();

        if ($filter_enabled) {
            $netify_fwa_running = $this->netify_fwa->get_running_state();
            $status['status'] = $this->netify_fwa->get_status();
        } else {
            $status['status'] = Daemon_Class::STATUS_STOPPED;
        }

        echo json_encode($status);
    }

    /**
     * Start.
     *
     * @return view
     */

    function start()
    {
        $this->load->library('protocol_filter/Protocol_Filter');
        $this->load->library('firewall/Firewall');

        try {
            $this->protocol_filter->set_state(TRUE);
            $this->protocol_filter->update_state();

            $this->firewall->restart(TRUE);
        } catch (Exception $e) {
            // Keep going
        }
    }

    /**
     * Stop.
     *
     * @return view
     */

    function stop()
    {
        $this->load->library('protocol_filter/Protocol_Filter');
        $this->load->library('firewall/Firewall');

        try {
            $this->protocol_filter->set_state(FALSE);
            $this->protocol_filter->update_state();

            $this->firewall->restart(TRUE);
        } catch (Exception $e) {
            // Keep going
        }
    }
}
