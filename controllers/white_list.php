<?php

/**
 * Protocol filter white list controller.
 *
 * @category   apps
 * @package    protocol-filter
 * @subpackage controllers
 * @author     eGloo <team@egloo.ca>
 * @copyright  2016 eGloo
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       https://www.netify.ai/resources/platforms/clearos
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * Protocol filter white list controller.
 *
 * @category   apps
 * @package    protocol-filter
 * @subpackage controllers
 * @author     eGloo <team@egloo.ca>
 * @copyright  2016 eGloo
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       https://www.netify.ai/resources/platforms/clearos
 */

class White_List extends ClearOS_Controller
{
    /**
     * White list default controller
     *
     * @return view
     */

    function index()
    {
        // Load libraries
        //---------------

        $this->load->library('protocol_filter/Protocol_Filter');
        $this->lang->load('protocol_filter');
        $this->lang->load('netify');

        // Load view data
        //---------------

        try {
            $data['white_list'] =  $this->protocol_filter->get_ip_whitelist();
        } catch (Exception $e) {
            $this->page->view_exception($e);
            return;
        }

        // Load views
        //-----------

        $this->page->view_form('white_list', $data, lang('netify_white_list'));
    }

    /**
     * Add entry view.
     *
     * @param string $ip IP address
     *
     * @return view
     */

    function add($ip = NULL)
    {
        // Load libraries
        //---------------

        $this->load->library('protocol_filter/Protocol_Filter');
        $this->lang->load('protocol_filter');
        $this->lang->load('netify');

        // Set validation rules
        //---------------------

        $this->form_validation->set_policy('ip', 'protocol_filter/Protocol_Filter', 'validate_ip', TRUE, TRUE);
        $form_ok = $this->form_validation->run();

        // Handle form submit
        //-------------------

        if ($this->input->post('submit') && ($form_ok === TRUE)) {
            try {

                $this->protocol_filter->add_whitelist_ip($this->input->post('ip'));
                $this->protocol_filter->update_state();

                $this->page->set_status_added();
                redirect('/protocol_filter/white_list');
            } catch (Exception $e) {
                $this->page->view_exception($e);
                return;
            }
        }

        // Load the views
        //---------------

        $this->page->view_form('add_edit', array(), lang('netify_white_list'));
    }

    /**
     * Delete entry view.
     *
     * @param string $ip IP address
     *
     * @return view
     */

    function delete($ip = NULL)
    {
        $confirm_uri = '/app/protocol_filter/white_list/destroy/' . $ip;
        $cancel_uri = '/app/protocol_filter/white_list';
        $items = array($ip);

        $this->page->view_confirm_delete($confirm_uri, $cancel_uri, $items);
    }

    /**
     * Destroys DNS entry view.
     *
     * @param string $ip IP address
     *
     * @return view
     */

    function destroy($ip = NULL)
    {
        // Load libraries
        //---------------

        $this->load->library('protocol_filter/Protocol_Filter');
        $this->lang->load('protocol_filter');

        // Handle delete
        //--------------

        try {
            $this->protocol_filter->delete_whitelist_ip($ip);
            $this->protocol_filter->update_state();

            $this->page->set_status_deleted();
            redirect('/protocol_filter/white_list');
        } catch (Exception $e) {
            $this->page->view_exception($e);
            return;
        }
    }
}
